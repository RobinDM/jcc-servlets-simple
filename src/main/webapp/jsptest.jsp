<%@ page import="java.time.LocalDateTime" %>
<%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 11/10/19
  Time: 13:22
  To change this template use File | Settings | File Templates.

  Browse to .../jsptest.jsp
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>HelloJsp</title>
</head>
<body>
<h1>
    Hello world from JSP
</h1>
<div>
    The current time is <%= LocalDateTime.now()
                                         .toString()%>
</div>
</body>
</html>

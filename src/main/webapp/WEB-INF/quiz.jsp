<%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 11/10/19
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Quiz</title>
</head>
<body>
<div>
    <h3>Translate from NL to EN:</h3>
</div>
<div>
    <form method="post">
        <label>
            <%= request.getAttribute("challenge") %>
            <input type="text" name="input"/>
        </label>
        <input type="submit" value="Submit"/>
    </form>
</div>
<div>
    <p>Your score is <%= request.getAttribute("success") %>/<%= request.getAttribute("attempts") %></p>
</div>

</body>
</html>

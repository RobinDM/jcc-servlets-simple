<%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 11/10/19
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="userbean" class="com.realdolmen.jcc.model.User" scope="session"/>
<html>
<head>
    <title>User page</title>
</head>
<body>
<p>
    Original message: <%= request.getParameter("name") %>
</p>
<p>
    Transformed message: <%= request.getAttribute("upper") %>
</p>
<p>
    From bean: <jsp:getProperty name="userbean" property="upperName"/>
</p>
</body>
</html>

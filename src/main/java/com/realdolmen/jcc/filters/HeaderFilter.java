package com.realdolmen.jcc.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(urlPatterns = "/headerfilter")
public class HeaderFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletResponse.setContentType("text/html");
        PrintWriter out = servletResponse.getWriter();
        out.println("<head>");
        out.println("<body>");
        out.println("<h1>");
        out.println("Fancy header!");
        out.println("</h1>");
        filterChain.doFilter(servletRequest, servletResponse);
        out.println("</body>");
        out.println("</head>");
    }

    @Override
    public void destroy() {

    }
}

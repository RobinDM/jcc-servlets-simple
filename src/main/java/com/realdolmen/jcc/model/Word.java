package com.realdolmen.jcc.model;

import java.util.Objects;

public class Word {
    private Word.Key id;
    private String dutch;
    private String english;

    public Word(Key id, String dutch, String english) {
        this.id = id;
        this.dutch = dutch;
        this.english = english;
    }

    public Key getId() {
        return id;
    }

    public void setId(Key id) {
        this.id = id;
    }

    public String getDutch() {
        return dutch;
    }

    public void setDutch(String dutch) {
        this.dutch = dutch;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Word word = (Word) o;
        return id == word.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Word{" + "id=" + id + ", dutch='" + dutch + '\'' + ", english='" + english + '\'' + '}';
    }

    public static class Key{
        private int value;

        public Key(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            Key key = (Key) o;
            return value == key.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }
}

package com.realdolmen.jcc.model;

import java.io.Serializable;

public class User implements Serializable {
    private String upperName;

    public User() {
    }

    public User(String upperName) {
        this.upperName = upperName;
    }

    public String getUpperName() {
        return upperName;
    }

    public void setUpperName(String upperName) {
        this.upperName = upperName;
    }
}

package com.realdolmen.jcc.services;

import com.realdolmen.jcc.model.Word;

import java.util.List;

public interface WordRepository {
    List<Word> getWords();

    Word getWordById(int id);

    void updateWord(Word word);

    void close();

    Word createWord(String dutch, String english);
}

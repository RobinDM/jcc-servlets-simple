package com.realdolmen.jcc.services;

import com.realdolmen.jcc.model.Word;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcWordRepository implements WordRepository {

    private static final String jdbcUrl = "jdbc:mysql://localhost:3306/servlets?user=dev&password=test&useLegacyDatetimeCode=false";
    private final HikariDataSource dataSource;

    public JdbcWordRepository(String jdbcUrl) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(jdbcUrl);
        this.dataSource = new HikariDataSource(hikariConfig);
    }

    private static JdbcWordRepository instance;

    public static JdbcWordRepository getInstance(){
        if (instance == null) {
            instance = new JdbcWordRepository(jdbcUrl);
        }
        return instance;
    }

    @Override
    public List<Word> getWords() {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select id, dutch, english from words");
            ResultSet resultSet = ps.executeQuery();
            List<Word> words = new ArrayList<>();
            while (resultSet.next()) {
                Word.Key id = new Word.Key(resultSet.getInt("id"));
                String dutch = resultSet.getString("dutch");
                String english = resultSet.getString("english");
                words.add(new Word(id, dutch, english));
            }
            return words;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Word getWordById(int id) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select dutch, english from words where id = ?");
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            // expect 1 result
            resultSet.next();
            Word.Key key = new Word.Key(id);
            String dutch = resultSet.getString("dutch");
            String english = resultSet.getString("english");
            return new Word(key, dutch, english);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateWord(Word word) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void close() {
        dataSource.close();
    }

    @Override
    public Word createWord(String dutch, String english) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("insert into words (dutch, english) values (?, ?);", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, dutch);
            ps.setString(2, english);
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            // expect 1 result
            resultSet.next();
            Word.Key key = new Word.Key(resultSet.getInt(1));
            return new Word(key, dutch, english);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

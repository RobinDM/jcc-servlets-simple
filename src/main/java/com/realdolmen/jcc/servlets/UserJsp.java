package com.realdolmen.jcc.servlets;

import com.realdolmen.jcc.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Browse to .../user?name=yourname
 */
@WebServlet(urlPatterns = "/userjsp")
public class UserJsp extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String upperName = name.toUpperCase();
        req.setAttribute("upper", upperName);
        req.setAttribute("upperName", upperName);
        req.getSession().setAttribute("userbean", new User(upperName));
        req.getRequestDispatcher("/WEB-INF/user.jsp")
           .forward(req, resp);
    }
}

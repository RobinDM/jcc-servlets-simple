package com.realdolmen.jcc.servlets;

import com.realdolmen.jcc.model.Word;
import com.realdolmen.jcc.services.JdbcWordRepository;
import com.realdolmen.jcc.services.WordRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet(urlPatterns = "/quiz")
public class Quiz extends HttpServlet {

    private static final String ATTEMPTS_COUNT = "attempts";
    private static final String SUCCESS_COUNT = "success";
    private static final String CHALLENGE = "challenge";

    private final WordRepository wordRepository = JdbcWordRepository.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer attemptCount = (Integer) req.getSession()
                                            .getAttribute(ATTEMPTS_COUNT);
        if (attemptCount == null) {
            attemptCount = 0;
            req.getSession()
               .setAttribute(ATTEMPTS_COUNT, attemptCount);
        }
        Integer successCount = (Integer) req.getSession()
                                            .getAttribute(SUCCESS_COUNT);
        if (successCount == null) {
            successCount = 0;
            req.getSession()
               .setAttribute(SUCCESS_COUNT, successCount);
        }

        List<Word> words = wordRepository.getWords();
        Collections.shuffle(words);
        Word randomWord = words.get(0);

        req.getSession()
           .setAttribute(CHALLENGE, randomWord);

        req.setAttribute(CHALLENGE, randomWord.getDutch());
        req.setAttribute(ATTEMPTS_COUNT, attemptCount);
        req.setAttribute(SUCCESS_COUNT, successCount);
        req.getRequestDispatcher("/WEB-INF/quiz.jsp")
           .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Word randomWord = (Word) req.getSession()
                                    .getAttribute(CHALLENGE);
        Integer ac = (Integer) req.getSession()
                                  .getAttribute(ATTEMPTS_COUNT);
        Integer sc = (Integer) req.getSession()
                                  .getAttribute(SUCCESS_COUNT);
        ac++;
        if (randomWord.getEnglish()
                      .equals(input)) {
            sc++;
        }
        req.getSession()
           .setAttribute(ATTEMPTS_COUNT, ac);
        req.getSession()
           .setAttribute(SUCCESS_COUNT, sc);
        resp.sendRedirect(req.getContextPath() + "/quiz");
    }
}

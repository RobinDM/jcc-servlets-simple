package com.realdolmen.jcc.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/html")
public class HtmlServlet extends HttpServlet {

    private static final String HTML_CONTENT_TYPE = "text/html";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType(HTML_CONTENT_TYPE);
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>");
        out.println("Hello World");
        out.println("</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>");
        out.println("Hello world!");
        out.println("</h1>");
        out.println("<p>");
        out.println("... from HtmlServlet");
        out.println("</p>");
        out.println("</body>");
        out.println("</html>");
    }
}

package com.realdolmen.jcc.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet(urlPatterns = "/cookies")
public class RepeatVisitorServlet extends HttpServlet {

    private static final String COOKIE_ID = "repeat-visit-identification";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        boolean isFirstVisit = Arrays.stream(req.getCookies())
                                     .noneMatch(c -> COOKIE_ID.equals(c.getName()));
        if (isFirstVisit) {
            Cookie c = new Cookie(COOKIE_ID, String.valueOf(System.nanoTime()));
            resp.addCookie(c);
            writer.println("Welcome Aboard!");
        } else {
            writer.println("Welcome Back!");
        }
    }
}

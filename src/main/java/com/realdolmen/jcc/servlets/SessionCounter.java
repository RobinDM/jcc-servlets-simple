package com.realdolmen.jcc.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Load the page and hit F5 a few times to see the counter to go.
 * Then, open an incognito/private window and visit the page again to notice that the counter has reset.
 */
@WebServlet(urlPatterns = "/counter")
public class SessionCounter extends HttpServlet {

    private static final String COUNTER_ATTRIBUTE_ID = "counter";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        // apparently, `isNew` does not work this way!
//        Integer counter = session.isNew() ? new Integer(0) : (Integer) session.getAttribute(COUNTER_ATTRIBUTE_ID);
        Integer counter = session.getAttribute(COUNTER_ATTRIBUTE_ID) == null ? new Integer(0) : (Integer) session.getAttribute(COUNTER_ATTRIBUTE_ID);
        counter++;
        session.setAttribute(COUNTER_ATTRIBUTE_ID, counter);
        resp.getWriter()
            .println("This is page load nr " + counter + "!");
    }
}

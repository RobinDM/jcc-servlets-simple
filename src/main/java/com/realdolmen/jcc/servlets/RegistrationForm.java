package com.realdolmen.jcc.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@WebServlet(urlPatterns = "/register")
public class RegistrationForm extends HttpServlet {

    private static final String COOKIE_ID = "register-identification";

    private final Map<String, FormParameters> state = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Optional<Cookie> first = Arrays.stream(req.getCookies())
                                       .filter(c -> COOKIE_ID.equals(c.getName()))
                                       .findFirst();
        Cookie c = first.isPresent() ? first.get() : new Cookie(COOKIE_ID, String.valueOf(System.nanoTime()));
        if (!state.containsKey(c.getValue())) {
            resp.addCookie(c);
            state.put(c.getValue(), new FormParameters());
        }
        FormParameters formParameters = state.get(c.getValue());
        PrintWriter writer = resp.getWriter();
        resp.setContentType("text/html");
        writer.println("<html>");
        writer.println("<body>");
        writer.println("<form method=\"post\">");
        writer.println("<input type=\"text\" name=\"firstname\" value=\"" + formParameters.getFirstName() + "\">");
        writer.println("</input>");
        writer.println("<input type=\"text\" name=\"lastname\" value=\"" + formParameters.getLastName() + "\">");
        writer.println("</input>");
        writer.println("<input type=\"text\" name=\"email\" value=\"" + formParameters.getEmail() + "\">");
        writer.println("</input>");
        writer.println("<input type=\"submit\" name=\"Submit\">");
        writer.println("</input>");
        writer.println("</form>");
        writer.println("</body>");
        writer.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Arrays.stream(req.getCookies())
              .filter(c -> COOKIE_ID.equals(c.getName()))
              .findFirst()
              .ifPresent(c -> {
                  FormParameters formParameters = state.get(c.getValue());
                  formParameters.setFirstName(req.getParameter("firstname"));
                  formParameters.setLastName(req.getParameter("lastname"));
                  formParameters.setEmail(req.getParameter("email"));
              });
        resp.getWriter().println("Input stored, reload the page!");
    }

    private class FormParameters {
        private String firstName;
        private String lastName;
        private String email;

        public FormParameters() {
            this("", "", "");
        }

        public FormParameters(String firstName, String lastName, String email) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}

package com.realdolmen.jcc.servlets;

import com.realdolmen.jcc.model.Word;
import com.realdolmen.jcc.services.JdbcWordRepository;
import com.realdolmen.jcc.services.WordRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/dictionary")
public class Dictionary extends HttpServlet {


    private final WordRepository wordRepository = JdbcWordRepository.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Word> words = wordRepository.getWords();
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>");
        out.println("Dictionary");
        out.println("</h1>");
        out.println("<table>");
        out.println("<tr>");
        out.println("<th>");
        out.println("id");
        out.println("</th>");
        out.println("<th>");
        out.println("Dutch");
        out.println("</th>");
        out.println("<th>");
        out.println("English");
        out.println("</th>");
        out.println("</tr>");
        for (Word word : words) {
            out.println("<tr>");
            out.println("<td>");
            out.println(word.getId()
                            .getValue());
            out.println("</td>");
            out.println("<td>");
            out.println(word.getDutch());
            out.println("</td>");
            out.println("<td>");
            out.println(word.getEnglish());
            out.println("</td>");
            out.println("</tr>");
        }
        out.println("</table>");
        out.println("</body>");
        out.println("</html>");
    }

    @Override
    public void destroy() {
        wordRepository.close();
    }
}

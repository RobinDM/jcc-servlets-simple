package com.realdolmen.jcc.servlets;

import com.realdolmen.jcc.services.JdbcWordRepository;
import com.realdolmen.jcc.services.WordRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/addword")
public class Form extends HttpServlet {

    private final WordRepository wordRepository = JdbcWordRepository.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>");
        out.println("Add word");
        out.println("</h1>");
        // no action means the same url as the current url
        out.println("<form method=\"post\">");
        out.println("<input type=\"text\" name=\"dutch\">");
        out.println("</input>");
        out.println("<input type=\"text\" name=\"english\">");
        out.println("</input>");
        out.println("<input type=\"submit\" value=\"Add\">");
        out.println("</input>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String dutch = req.getParameter("dutch");
        String english = req.getParameter("english");
        wordRepository.createWord(dutch, english);
        resp.sendRedirect(req.getContextPath() + "/dictionary");
    }
}
